import * as plugins from './qonto.plugins';
import * as interfaces from './interfaces'
import { QontoBankAccount } from './qonto.classes.bankaccount';

export interface IQontoTransactionOptions {
  id: string;
  date: number;
  amount: number;
  name: string;
  description: string;
  justForLooks?: {
    isoDate: string;
  };
}

export class QontoTransaction implements IQontoTransactionOptions {
  // STATIC
  public static fromApiObject(qontoBankAccountRefArg: QontoBankAccount, apiObjectArg: interfaces.IQontoTransactionApiResponse['transactions'][0]) {
    let transactionAmount = apiObjectArg.amount;
    if (apiObjectArg.side === 'debit') {
      transactionAmount = transactionAmount * -1;
    }
    const returnQontoTransaction =  new QontoTransaction(qontoBankAccountRefArg, {
      id: apiObjectArg.transaction_id,
      date: new Date(apiObjectArg.emitted_at).getTime(),
      amount: transactionAmount,
      description: `${apiObjectArg.reference ? apiObjectArg.reference : apiObjectArg.operation_type}`,
      name: apiObjectArg.label,
      justForLooks: {
        isoDate: apiObjectArg.emitted_at
      }
    });
    return returnQontoTransaction;
  }

  // INSTANCE
  qontoBankAccountRef: QontoBankAccount;

  // data
  id: string;
  date: number;
  amount: number;
  name: string;
  description: string;
  justForLooks?: {
    isoDate: string;
  };

  constructor(qontoBankAccountRefArg, optionsArg: IQontoTransactionOptions) {
    this.qontoBankAccountRef = qontoBankAccountRefArg;
    Object.assign(this, optionsArg);
  }
}
