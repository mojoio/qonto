import * as plugins from './qonto.plugins';
import * as interfaces from './interfaces';
import { QontoBankAccount } from './qonto.classes.bankaccount';

export interface IQontoAccountOptions {
  qontoUser: string;
  qontoToken: string;
}

export class QontoAccount {
  private apiBaseDomain = 'https://thirdparty.qonto.com/v2';
  private options: IQontoAccountOptions;

  constructor(optionsArg: IQontoAccountOptions) {
    this.options = optionsArg;
  }

  public async getBankAccounts() {
    const apiResponse: interfaces.IQontoOrganizaionApiResponse = await this.request('GET', `/organizations/${this.options.qontoUser}`);
    const returnBankAccounts: QontoBankAccount[] = [];
    for (const apiBankAccountObject of apiResponse.organization.bank_accounts) {
      returnBankAccounts.push(QontoBankAccount.fromApiBankAccountObject(this, apiBankAccountObject));
    }
    return returnBankAccounts;
  }

  public async request(methodArg: 'GET', routeArg: string) {
    const response = await plugins.smartrequest.request(`${this.apiBaseDomain}${routeArg}`, {
      method: methodArg,
      headers: {
        // Accept: 'application/json',
        // 'Content-Type': 'application/json',
        Authorization: `${this.options.qontoUser}:${this.options.qontoToken}`
      },
      keepAlive: false
    });
    if (response.statusCode > 299) {
      console.log(response.body);
    }
    return response.body;
  }
}